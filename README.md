# OmicsMaps

Code for plotting RNA-seq and proteomics data on Escher metabolic maps  
Map files produced so far:  L:\MBC_Omics\RNASeq\Escher Maps  
Code for producing maps:  L:\ComputationalBiology\Jeff Orth\Omics Data Mapping  


## Important files:
* omicsmaps.py – python module with functions for mapping RNAseq or proteomics data to Mcap Escher maps
* M_capsulatus_v1.13_rBN_RNAseq2_model.json – Mcap metabolic model file in json format (can be loaded by Escher) with heterologous genes (identifiable by “h_” prefix in gene names) for mapping omics data.
* Mcap_model_transcripts_mapping_v3.txt – text file with mapping from model gene IDs (column 1) to data gene IDs (column 2).  When analyzing data with new heterologous genes not already on this list, edit this file.
* plot_RNASEQ-MCAP-0015-0025.ipynb – a good example notebook for mapping RNAseq data.  Makes maps for both TPMs (relative expression of all genes for one timepoint/condition) and DESEQ fold changes (relative expression between two timepoints/conditions).
* save_ProtExp022.sql – a good example notebook for mapping proteomics data.  Makes maps for both NSAFs (relative abundance of all proteins for one timepoint/condition) and fold changes (relative expression between two timepoints/conditions).


## Workflows:

### Plotting RNAseq TPM maps (each map shows relative expression at one timepoint)
1.	Pull TPM data from database (ask Abhinav, database changed since last time I did this), save each condition (including replicates) in individual text files.  This step is not yet automated, use SQLplus, SQL Developer, or whatever you want
2.	Use omicsmaps.map_TPM_data() to average replicate TPMs for all genes and map to model gene IDs, returns a dictionary.  When a dataset contains more than one heterologous gene for one metabolic reaction (this is not common), the average TPMs for these genes are summed and linked to one dummy h_ gene in the model.
3.	Use omicsmaps.plot_TPM_map() to map this data on an Escher map, writes maps as html files.

### Plotting RNAseq fold change maps
1.	Pull log2 and raw fold change data (including p-values) from database (ask Abhinav, database changed since last time I did this), save each comparison in individual text files.
2.	Use omicsmaps.map_FC_data to map the fold changes to model gene IDs.  Returns a dictionary of log2 fold changes, a dictionary of absolute fold changes, a dictionary of 1’s (Escher needs this to properly plot fold changes instead of absolute expression values), and a dictionary of p-values.
3.	Use omicsmaps.plot_FC_map() with the absolute fold changes dictionary to map these on an Escher map.  Use omicsmaps.plot_log2FC_map() with the log2 fold changes dictionary to plot these on an Escher map.  Use the optional pval_cutoff input to set an upper limit to p-values that will be plotted.  Any fold change with a p-value above this cutoff is not plotted on the map.  Both functions write maps as html files.

### Plotting proteomics NSAF maps
1.	Pull proteomics NSAF data from database (ask Abhinav, database changed since last time I did this), save each condition (including replicates) in individual text files.
2.	Use omicsmaps.map_NSAF_data() to average replicate NSAFs for all genes and map to model gene IDs, returns a dictionary.  When a dataset contains more than one heterologous gene for one metabolic reaction (this is not common), the average NSAFs for these genes are summed and linked to one dummy h_ gene in the model.
3.	Use omicsmaps.plot_NSAF_map() to map this data on an Escher map, writes maps as html files.

### Plotting proteomics NSAF fold change maps (proteomics fold changes usually aren’t calculated and saved to the database like they are for RNAseq, we are going to let Escher calculate fold changes itself)
1.	Pull proteomics NSAF data from database (ask Abhinav, database changed since last time I did this), save each condition (including replicates) in individual text files.
2.	Use omicsmaps.map_NSAF_data() to average replicate NSAFs for all genes and map to model gene IDs, returns a dictionary.
3.	Use omicsmaps.plot_NSAF_FC_map() with the NSAF dictionaries for both conditions to compare as inputs.  Escher will calculate the fold changes.  Writes maps as html files.

