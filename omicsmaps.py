import pandas as pd
import escher

"""
Functions for plotting RNAseq and proteomics data to Escher metabolic maps

Jeff Orth  10/10/18
"""

# heterologous genes
# edit this list to add more genes for new strains
h_genes = ['h_alsS',
		   'h_ilvC',
		   'h_ilvD',
		   'h_kdc',
		   'h_adh_iboh_x',
		   'h_adh_iboh_y',
		   'h_budA',
		   'h_adh_23bdo_y',
		   'h_acs',
		   'h_por',
		   'h_fpr',
		   'h_fer',
		   'h_gltA',
		   'h_aceA',
		   'h_maeB',
		   'h_glcB']



# map_TPM_data
# average TPMs from an RNAseq datafile and map to model genes, return a dictionary
def map_TPM_data(model, mapping_table, file_path):
	
	data_table = pd.read_table(file_path, delimiter='\t', header=0)

	# average TPM from multiple samples
	mapping_table_remove_0 = mapping_table.copy()
	exp_values = []
	for gene in mapping_table['Transcripts']:
		dt2 = data_table[data_table['TRANSCRIPT'].isin([gene])]
		if len(dt2['TPM']) == 0:
			mapping_table_remove_0 = mapping_table_remove_0[mapping_table_remove_0['Transcripts'] != gene]
		else:
			avg_tpm = sum(dt2['TPM'])/len(dt2['TPM'])
			exp_values.append(avg_tpm)

	# remove heterologous genes for the same reaction (need to keep only one for mapping)
	mapping_table_remove_h = mapping_table_remove_0.copy()
	for h_gene in h_genes:
		mt2 = mapping_table_remove_h[mapping_table_remove_h['Model genes'].isin([h_gene])]
		if len(mt2) > 1:
			index = mt2.index.tolist()
			sum_h_genes = 0
			for i in index:
				sum_h_genes = sum_h_genes + exp_values[i]
			exp_values[index[0]] = sum_h_genes
			for i in index[1:]:
				mapping_table_remove_h = mapping_table_remove_h.drop(i)
				exp_values.pop(index[1])
		mapping_table_remove_h = mapping_table_remove_h.reset_index(drop=True)

	gene_TPM_dict = dict(zip(mapping_table_remove_h['Model genes'],exp_values))

	return gene_TPM_dict



# map_FC_data
# Pull log2 and raw fold changes from an RNAseq datafile and map to model genes, return a
# dictionary. Also returns a dictionary of genes (keys) and ones, needed as an input by Escher to
# properly plot fold changes
def map_FC_data(model, mapping_table, file_path):

	data_table = pd.read_table(file_path,delimiter='\t',header=0)

	# get log2 and raw fold changes
	exp_values_log2 = []
	exp_values_fc = []
	exp_values_padj = []
	mapping_table_remove_0 = mapping_table.copy()
	for gene in mapping_table['Transcripts']:
		dt2 = data_table[data_table['TRANSCRIPT'].isin([gene])]
		if len(dt2['LOG2FC']) == 0:
			mapping_table_remove_0 = mapping_table_remove_0[mapping_table_remove_0['Transcripts'] != gene]
			#print('dropped gene ',gene)
		else:
			log2fc = dt2.loc[dt2.index[0],'LOG2FC']
			fc = 2**log2fc
			padj = dt2.loc[dt2.index[0],'PADJ']
			exp_values_log2.append(log2fc)
			exp_values_fc.append(fc)
			exp_values_padj.append(padj)

	# remove heterologous genes for the same reaction (need to keep only one for mapping)
	mapping_table_remove_h = mapping_table_remove_0.copy()
	for h_gene in h_genes:
		mt2 = mapping_table_remove_h[mapping_table_remove_h['Model genes'].isin([h_gene])]
		if len(mt2) > 1:
			index = mt2.index.tolist()
			# replace multiple log2fcs and pvals with the largest of the log2 fold changes
			h_genes_log2 = 0.0
			h_genes_fc = 0.0
			h_genes_padj = 0.0
			for i in index:
				if abs(exp_values_log2[i]) > abs(h_genes_log2):
					h_genes_log2 = exp_values_log2[i]
					h_genes_fc = exp_values_fc[i]
					h_genes_padj = exp_values_padj[i]
			exp_values_log2[index[0]] = h_genes_log2
			exp_values_fc[index[0]] = h_genes_fc
			exp_values_padj[index[0]] = h_genes_padj
			for i in index[1:]:
				mapping_table_remove_h = mapping_table_remove_h.drop(i)
				exp_values_log2[i:i+1] = []
				exp_values_fc[i:i+1] = []
				exp_values_padj[i:i+1] = []
		mapping_table_remove_h = mapping_table_remove_h.reset_index(drop=True)

	ones = [1.0]*len(exp_values_fc)
	ones_dict = dict(zip(mapping_table_remove_h['Model genes'],ones))
	gene_log2_dict = dict(zip(mapping_table_remove_h['Model genes'],exp_values_log2))
	gene_fc_dict = dict(zip(mapping_table_remove_h['Model genes'],exp_values_fc))
	gene_pval_dict = dict(zip(mapping_table_remove_h['Model genes'],exp_values_padj))

	return [gene_log2_dict, gene_fc_dict, ones_dict, gene_pval_dict]



# map_NSAF_data
# average percent NSAF values from a proteomics datafile and map to model genes, return a dictionary
def map_NSAF_data(model, mapping_table, file_path):
	
	data_table = pd.read_table(file_path, delimiter='\t', header=0)

	# average pct NSAF from multiple samples
	mapping_table_remove_0 = mapping_table.copy()
	exp_values = []
	for protein in mapping_table['Transcripts']:
		dt2 = data_table[data_table['PROTEIN_ACCESSION'].isin([protein])]
		if len(dt2['PCT_NSAF']) == 0:
			mapping_table_remove_0 = mapping_table_remove_0[mapping_table_remove_0['Transcripts'] != protein]
		else:
			avg_nsaf = sum(dt2['PCT_NSAF'])/len(dt2['PCT_NSAF'])
			exp_values.append(avg_nsaf)

	# remove heterologous genes for the same reaction (need to keep only one for mapping)
	mapping_table_remove_h = mapping_table_remove_0.copy()
	for h_gene in h_genes:
		mt2 = mapping_table_remove_h[mapping_table_remove_h['Model genes'].isin([h_gene])]
		if len(mt2) > 1:
			index = mt2.index.tolist()
			sum_h_genes = 0
			for i in index:
				sum_h_genes = sum_h_genes + exp_values[i]
			exp_values[index[0]] = sum_h_genes
			for i in index[1:]:
				mapping_table_remove_h = mapping_table_remove_h.drop(i)
				exp_values.pop(index[1])
		mapping_table_remove_h = mapping_table_remove_h.reset_index(drop=True)

	prot_NSAF_dict = dict(zip(mapping_table_remove_h['Model genes'],exp_values))

	return prot_NSAF_dict



# merge_gene_dicts
# combine two or more TPM or NSAF gene dictionaries by averaging the values for each gene
# input: list of gene_TPM_dicts or prot_NSAF_dicts
def merge_gene_dicts(gene_dicts):

	# list all unique genes/proteins in all dictionaries
	all_genes = []
	for gene_dict in gene_dicts:
	    for gene in gene_dict.keys():
	        if gene not in all_genes:
	            all_genes.append(gene)

	# average TPM/NSAF for each protein
	avg_gene_dict = {}
	for gene in all_genes:
	    vals = []
	    for gene_dict in gene_dicts:
	        if gene in gene_dict:
	            vals.append(gene_dict[gene])
	    val = sum(vals)/len(vals)
	    avg_gene_dict[gene] = val

	return avg_gene_dict



# plot_TPM_map
# plot the TPM data from map_TPM_data on an Escher map
def plot_TPM_map(model, map_file, gene_TPM_dict, map_path):

	# color scaling
	reaction_styles=['color', 'size', 'text']
	reaction_scale = [{'type':'min', 'color':'blue', 'size':24},
					  {'type':'Q1', 'color':'#000088', 'size':24},
					  {'type':'median', 'color':'black', 'size':24},
					  {'type':'Q3', 'color':'#880000', 'size':24},
					  {'type':'max', 'color':'red', 'size':24}]

	# make Escher map and save
	data_map = escher.Builder(map_json=map_file, model=model, gene_data=gene_TPM_dict,
							 reaction_styles=reaction_styles, reaction_scale=reaction_scale,
							 show_gene_reaction_rules=True, gene_font_size=12)
	data_map.save_html(filepath=map_path)



# plot_NSAF_map
# plot the percent NSAF data from map_NSAF_data on an Escher map
def plot_NSAF_map(model, map_file, prot_NSAF_dict, map_path):

	# color scaling
	reaction_styles=['color', 'size', 'text']
	reaction_scale = [{'type':'min', 'color':'blue', 'size':24},
					  {'type':'Q1', 'color':'#000088', 'size':24},
					  {'type':'median', 'color':'black', 'size':24},
					  {'type':'Q3', 'color':'#880000', 'size':24},
					  {'type':'max', 'color':'red', 'size':24}]

	# make Escher map and save
	data_map = escher.Builder(map_json=map_file, model=model, gene_data=prot_NSAF_dict,
							 reaction_styles=reaction_styles, reaction_scale=reaction_scale,
							 show_gene_reaction_rules=True, gene_font_size=12)
	data_map.save_html(filepath=map_path)



# plot_FC_map
# plot the fold change data from map_FC_data on an Escher map
def plot_FC_map(model, map_file, gene_fc_dict, ones_dict, gene_pval_dict, map_path, pval_cutoff=0):

	# remove genes with p values above cutoff
	gene_fc_dict_cutoff = gene_fc_dict.copy()
	ones_dict_cutoff = ones_dict.copy()
	if pval_cutoff > 0:
		for gene in gene_fc_dict.keys():
			pval = gene_pval_dict[gene]
			if pval > pval_cutoff:
				del gene_fc_dict_cutoff[gene]
				del ones_dict_cutoff[gene] 

	# colors
	reaction_styles=['color', 'size', 'text']
	# check if min or max fold changes after p-value filtering are < 0 or > 0, determines which
	# color scaling to use
	if 	(min(gene_fc_dict_cutoff.values()) < 1.0) and (max(gene_fc_dict_cutoff.values()) > 1.0):
		reaction_scale = [{'type':'min', 'color':'blue', 'size':24},
						  {'type':'value', 'value':0, 'color':'black', 'size':24},
						  {'type':'max', 'color':'red', 'size':24}]
	elif min(gene_fc_dict_cutoff.values()) > 1.0:
		reaction_scale = [{'type':'min', 'color':'black', 'size':24},
						  {'type':'max', 'color':'red', 'size':24}]
	else: # max < 0
		reaction_scale = [{'type':'min', 'color':'blue', 'size':24},
						  {'type':'max', 'color':'black', 'size':24}]

	# make Escher map and save
	fcones_dict = [ones_dict_cutoff, gene_fc_dict_cutoff]
	data_map = escher.Builder(map_json=map_file, model=model, gene_data=fcones_dict,
							 reaction_styles=reaction_styles, reaction_scale=reaction_scale,
							 show_gene_reaction_rules=True, gene_font_size=12,
							 reaction_compare_style='fold')
	data_map.save_html(filepath=map_path)



# plot_log2FC_map
# plot the fold change data from map_FC_data on an Escher map in log2 units
def plot_log2FC_map(model, map_file, gene_fc_dict, ones_dict, gene_pval_dict, map_path, pval_cutoff=0):

	# remove genes with p values above cutoff
	gene_fc_dict_cutoff = gene_fc_dict.copy()
	ones_dict_cutoff = ones_dict.copy()
	if pval_cutoff > 0:
		for gene in gene_fc_dict.keys():
			pval = gene_pval_dict[gene]
			if pval > pval_cutoff:
				del gene_fc_dict_cutoff[gene]
				del ones_dict_cutoff[gene] 

	# colors
	reaction_styles=['color', 'size', 'text']
	# check if min or max fold changes after p-value filtering are < 0 or > 0, determines which
	# color scaling to use
	if 	(min(gene_fc_dict_cutoff.values()) < 1.0) and (max(gene_fc_dict_cutoff.values()) > 1.0):
		reaction_scale = [{'type':'min', 'color':'blue', 'size':24},
						  {'type':'value', 'value':0, 'color':'black', 'size':24},
						  {'type':'max', 'color':'red', 'size':24}]
	elif min(gene_fc_dict_cutoff.values()) > 1.0:
		reaction_scale = [{'type':'min', 'color':'black', 'size':24},
						  {'type':'max', 'color':'red', 'size':24}]
	else: # max < 0
		reaction_scale = [{'type':'min', 'color':'blue', 'size':24},
						  {'type':'max', 'color':'black', 'size':24}]

	# make Escher map and save
	fcones_dict = [ones_dict_cutoff, gene_fc_dict_cutoff] 
	data_map = escher.Builder(map_json=map_file, model=model, gene_data=fcones_dict,
							 reaction_styles=reaction_styles, reaction_scale=reaction_scale,
							 show_gene_reaction_rules=True, gene_font_size=12,
							 reaction_compare_style='log2_fold')
	data_map.save_html(filepath=map_path)



# plot_NSAF_FC_map 
# calculate the fold changes from two sets fo percent NSAF data and plot on an Escher map
def plot_NSAF_FC_map(model, map_file, prot_NSAF_dict1, prot_NSAF_dict2, map_path):

	prot_fold_change_dict = [prot_NSAF_dict2, prot_NSAF_dict1]

	# colors
	reaction_styles=['color', 'size', 'text']
	reaction_scale = [{'type':'min', 'color':'blue', 'size':24},
	                  {'type':'value', 'value':-1, 'color':'black', 'size':24},
	                  {'type':'value', 'value':1, 'color':'black', 'size':24},
	                  {'type':'max', 'color':'red', 'size':24}]

	# make Escher map and save
	data_map = escher.Builder(map_json=map_file, model=model, gene_data=prot_fold_change_dict,
							 reaction_styles=reaction_styles, reaction_scale=reaction_scale,
							 show_gene_reaction_rules=True, gene_font_size=12,
							 reaction_compare_style='fold')
	data_map.save_html(filepath=map_path)


